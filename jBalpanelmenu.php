<?php
/**
 * jBalpanelmenu widget for yii
 *
 * This widget is a wrapper for jpanelmenu http://jpanelmenu.com/
 *
 * @author Jim Well Balatero(JbalTero) <jbaltero@gmail.com>
 * @version 1.0
 * @copyright Copyright &copy; 2014 by Jim Well Balatero(JbalTero)
 * @license GNU General Public License
 */
class jBalpanelmenu extends CWidget {

    /**
     * @var array jpanelmenu options http://jpanelmenu.com/#options
     */
    public $options = array();

    /**
     * @var array htmlOptions for the div of the menu
     */
    public $htmlOptions = array();

    /**
     * @var string html this will be used as the menu content
     */
    public $content = NULL;

    /**
     * @var bool: Set to TRUE if you want jpanelmenu to on() or init directly after instantiation.
     *          Set to FALSE if you don't want it to on() or init after instatiation.
     *          You need to explicitly call jPanelMenu.on() to initialize and show the menu.
     */
    public $startOn = TRUE;

    /**
     * @var bool Whether to force htmlOptions 'style' property to 'display: none;'
     *           Becareful on setting this to FALSE. If this is FALSE, it will use your defined
     *           'style' regardless if the style property has 'display: none' and will
     *           show/display $this->content
     *
     */
    public $forceDisplayNone = TRUE;

    /**
     * @var string Default Values !Don't change this!
     */
    private $_id = 'menu';
    private $_style = 'display: none;';
    private $_direction = 'left';

    public function init(){

        /**
         * Register required scripts
         */
        $assetsDir=(defined(__DIR__) ? __DIR__ : dirname(__FILE__)).'/assets';
        $assets=Yii::app()->assetManager->publish($assetsDir, false, -1, YII_DEBUG);
        $ext=defined('YII_DEBUG') ? 'js' : 'min.js';
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
            ->registerCssFile($assets . '/css/jpanelmenu.css')
            ->registerScriptFile($assets . '/js/jquery.jpanelmenu.'.$ext);

        /**
         *  set options['menu'] as htmlOptions['id']
         *  if htmlOptions['id'] is set and options['menu'] is not set
         */
        if ( isset( $this->htmlOptions[ 'id' ] )  && !isset( $this->options['menu'] ) && $this->htmlOptions[ 'id' ] != 'jPanelMenu-menu' )
            $this->options['menu'] = '#' . $this->htmlOptions[ 'id' ];

        /**
         *  set htmlOptions[ 'id' ] as options['menu']
         *  if htmlOptions['id'] is not set and options['menu'] is set
         */
        if ( !isset( $this->htmlOptions[ 'id' ] )  && isset( $this->options['menu'] ) ){
            if ( strpos( $this->options[ 'menu' ], '#' ) !== FALSE ) {
                $len = strlen( $this->options[ 'menu' ] );
                $this->options['menu'] = substr($this->options['menu'], -($len-1));
            }
            $this->htmlOptions[ 'id' ] = $this->options['menu'];
        }

        /**
         *  set htmlOptions[ 'id' ] to default value
         *  if htmlOptions[ 'id' ] is not set
         */
        if ( !isset( $this->htmlOptions[ 'id' ] ) )
            $this->htmlOptions[ 'id' ] = $this->_id;

        /**
         *  set options[ 'direction' ] to default
         *  if options[ 'direction' ] is not 'left' or 'right' values
         */
        if ( isset( $this->options[ 'direction' ] ) ) {
            if ( !in_array( $this->options[ 'direction' ], array( 'left', 'right' ) ) ) {
                $this->options[ 'direction' ] = $this->_direction;
            }
        }

        /**
         *  set htmlOptions[ 'style' ] to default value
         *  if htmlOptions[ 'style' ] is not set
         */
        if ( isset( $this->htmlOptions[ 'style' ] ) ){
            if ( $this->forceDisplayNone ) {
                $re       = '/([\s]*(?P<key>display)[\s]*:[\s]*(?P<value>none|block);[\s]*)/i';
                $replaced = preg_replace( $re, $this->_style, $this->htmlOptions[ 'style' ], -1, $count );

                if ( $count == 0 ) $replaced .= $this->_style;

                $this->htmlOptions[ 'style' ] = $replaced;
            }
        }
        else
            $this->htmlOptions[ 'style' ] = $this->_style;
    }

    public function run(){

        /**
         * TODO: Make sure the $this->content does not contain id
         *       so that it will not confuse with jpanelmenu
         * Next Release maybe
         */

        // Render the content
        echo CHtml::tag( 'div' , $this->htmlOptions, $this->content);

        // RegisterScript
        $script= 'var jPanelMenu = $.jPanelMenu('.CJavaScript::encode($this->options).');';
        if($this->startOn)
            $script .= 'jPanelMenu.on();';

        Yii::app()->clientScript->registerScript(__CLASS__.'#'.$this->id, $script, CClientScript::POS_END);
    }
}
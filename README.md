##Overview
This extension is a wrapper for the jpanelmenu jQuery plugin.

jPanelMenu v1.3.0 is a jQuery plugin that creates a paneled-style menu (like the type seen in the mobile versions of Facebook and Google, as well as in many native iPhone applications).

![Overview](http://i.imgur.com/hOcaGwd.jpg?1)

Does It Animate?
----------------

Of course! (If you want it to, there’s an [option](http://jpanelmenu.com/#options "options") for that.)

##Quick Installation

1. [Download](https://bitbucket.org/Codeitat-Software/jbalpanelmenu/) jBalpanelmenu
2. Extract to _protected/extensions_

##Usage

In your _protected/views/layouts/main.php_,
~~~
[php]
<?php
// RECOMMENDED before echo $content
$this->widget( 'ext.jBalpanelmenu.jBalpanelmenu', array(
    'id' => 'jpanelmenu',
    'content' => $this->renderPartial( '//layouts/jpanelmenu', array(), TRUE ),
    'options' => array(
        'direction'           => 'left',
        'openPosition'        => '22%',
        'duration'            => 500,
        'keyboardShortcuts'   => array(
            array( // escape key
                   'code'  => 27,
                   'open'  => FALSE,
                   'close' => TRUE,
            ),
            array( // left arrow key
                   'code'  => 37,
                   'open'  => TRUE,
                   'close' => FALSE,
            ),
            array( // right arrow key
                   'code'  => 39,
                   'open'  => FALSE,
                   'close' => TRUE,
            )
        ),
        // Other options: http://jpanelmenu.com/#options
    ),
) );
?>
~~~
- Create new _jpanelmenu.php_ file under _protected/views/layouts/_
- jpanelmenu.php will serve as your jpanelmenu content.
- You can put some widgets there or code your html.

You can try putting [CMenu](http://www.yiiframework.com/doc/api/1.1/CMenu) widget in _jpanelmenu.php_ like this...
~~~
[php]
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>array(
        array('label'=>'Home', 'url'=>'#'),
        array('label'=>'Products', 'url'=>'#'),
        array('label'=>'New Arrivals', 'url'=>'#'),
        array('label'=>'Most Popular', 'url'=>'#'),
        array('label'=>'Login', 'url'=>'#'),
        array('label'=>'Home', 'url'=>'#'),
        array('label'=>'Products', 'url'=>'#'),
        array('label'=>'New Arrivals', 'url'=>'#'),
        array('label'=>'Most Popular', 'url'=>'#'),
        array('label'=>'Login', 'url'=>'#'),
    ),
));
?>
~~~

##Resources
* Found a bug or want a feature? [Report it on bitbucket.org](https://bitbucket.org/Codeitat-Software/jbalpanelmenu/issues?status=new&status=open)
* [Code on bitbucket](https://bitbucket.org/Codeitat-Software/jbalpanelmenu/)
* E-Mail the author: JbalTero <[jbaltero@gmail.com](mailto:jbaltero@gmail.com)>
* [JPanelMenu/Demo](http://jpanelmenu.com/)